/**
 * Author:  Yihao Jiang
 * Created: Nov 3, 2016
 */

/*
Create view for Java EE authentication
*/
drop view jdbcrealm_user;
drop view jdbcrealm_group;

create view jdbcrealm_user (username, password) as
select username, password
from account;

create view jdbcrealm_group (username, groupname) as
select username, groupname
from account;

/*
insert default admin account
*/
INSERT INTO AIP.ACCOUNT (USERNAME, ACTIVE, EMAIL, FULLNAME, GROUPNAME, PASSWORD) 
	VALUES ('admin', 1, 'jiangyh91@gmail.com', NULL, 'Admins', '87b137273ad0ff57c58683291e947d239cffe6bb21992377df6f1486d475b088');
