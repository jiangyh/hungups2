package au.edu.uts.aip.hungups2.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

/**
 * The Account Entity class that maps all the attribute of an account
 * @author Yihao Jiang
 * 
 */
@Entity
@NamedQueries({
    // Find all people by their last name
    @NamedQuery(name="Account.findByUsername",
                query="select a from Account a where a.username=:username"),
    // Find all people
    @NamedQuery(name="Account.findAll",
                query="select a from Account a")
    
})
public class Account implements Serializable {
    
    private String username;
    private String password;
    private String fullname;
    private String email;
    private boolean active = false;

    private String groupname;
    
    private List<Image> images = new ArrayList<>();
    
    @Id
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Size(min=1)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @OneToMany(mappedBy="account", cascade=CascadeType.ALL)
    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    
    
}
