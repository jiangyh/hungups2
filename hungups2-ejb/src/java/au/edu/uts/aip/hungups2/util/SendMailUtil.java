package au.edu.uts.aip.hungups2.util;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * A utility class for sending verification or reset password emails
 * @author Harold
 */
public class SendMailUtil {
    
    public static final String HOST = "smtp.live.com";
    public static final String PROTOCOL = "smtp";
    public static final int NON_SSL_PORT = 587;
    public static final String SENDER = "haroldfrost1@hotmail.com";
    public static final String SENDERPWD = "vK$BkYQ63vS2b6n$";
    
    /**
     * Retrieve a session from the mail server using smtp
     * @return a session
     */
    public static Session getSession() { 
        Properties props = new Properties();  
        props.setProperty("mail.debug", "true");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.host", HOST);  
        props.setProperty("mail.transport.protocol", PROTOCOL);  
        props.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getInstance(props);  
        return session;
    }
    
    /**
     * Send an email to a user's registered email
     * @param receiver the user is receiving
     * @param content the content of the email
     * @throws AddressException
     * @throws MessagingException 
     */
    public static void send(String receiver, String content) throws AddressException, MessagingException{
        
        Session session = getSession();
        Message msg = new MimeMessage(session);  
        
        msg.setSubject("Hungups Email Verfication");  
        msg.setText(content);
        msg.setFrom(new InternetAddress(SENDER));  
        Transport transport = session.getTransport();  
        transport.connect(SENDER, SENDERPWD);  
        InternetAddress[] addrs = {new InternetAddress(receiver)};  
        transport.sendMessage(msg, addrs);  
        transport.close();   
    }
}
