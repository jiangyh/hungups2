/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package au.edu.uts.aip.hungups2.ejb;


import java.text.SimpleDateFormat;
import java.util.Date;
import au.edu.uts.aip.hungups2.entity.Account;
import au.edu.uts.aip.hungups2.entity.Image;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * an EJB to manage images information in the database
 * does not associate with any image files manipulation
 * @author Yihao Jiang
 */
@Stateless
public class ImageBean {
    
    @PersistenceContext
    private EntityManager em;
    
    /**
     * Retrieves all images in the database.
     * @return a list of image instances in the database
     */
    public List<Image> findAll() {
        TypedQuery<Image> query = em.createNamedQuery("Image.findAll",Image.class);
        return query.getResultList();
    }
    
    /**
     * Find an image by its primary key.
     * @param id the unique primary key for the image
     * @return the corresponding Image entity
     */
    public Image findImage(int id) {
        return em.find(Image.class, id);
    }
    
    /**
     * Upload an image to the database.
     * @param image the new image object upload to the database
     * @param username the username of account that uploads image 
     */
    public void uploadImage(Image image,String username) {
        Account managed = em.find(Account.class, username);
        
        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy- HH:mm:ss");//设置日期格式
        image.setUploadDate(new Date());// new Date()为获取当前系统时间
        
        image.setAccount(managed);
        managed.getImages().add(image);
        em.persist(image);
    }
    
    /**
     * Updates the image information of one image.
     * This method does not update the list of images.
     * @param image a image object containing the id of the image to 
     *                      update the image information 
     */
    public void updateImage(Image image) {
        em.merge(image);
    }
    
    /**
     * Deletes an image from the database.
     * @param image the object of the image to delete
     */
    public void deleteImage(Image image) {
        
        Image managed = em.find(Image.class, image.getId());
        managed.getAccount().getImages().remove(managed);
        if (managed != image) {
            image.getAccount().getImages().remove(image);
        }
        em.remove(managed);
    }
}
