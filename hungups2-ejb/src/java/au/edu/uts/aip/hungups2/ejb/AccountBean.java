package au.edu.uts.aip.hungups2.ejb;

import au.edu.uts.aip.hungups2.entity.Account;
import au.edu.uts.aip.hungups2.util.SendMailUtil;
import au.edu.uts.aip.hungups2.util.ShaUtil;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * An EJB to manipulate information of user accounts in the database
 * @author hfrost, Yihao Jiang
 */
@Stateless
public class AccountBean {

    @PersistenceContext
    private EntityManager em;

    private static final String HASH_KEY = "E3NxalgCeyv3gkkKuyU!K0r$ASNwbI1B15D^";

    /**
     * Retrieves all people in the database by username.
     *
     * @param username the username of entities to retrieve from the database
     * @return a list of all matching account instances in the database
     */
    public List<Account> findByUsername(String username) {
        TypedQuery<Account> query = em.createNamedQuery("Account.findByUsername", Account.class);
        query.setParameter("username", username);
        return query.getResultList();
    }

    /**
     * Find an account by their primary key.
     *
     * @param username the unique primary key for the person
     * @return the corresponding Person entity
     */
    public Account findAccount(String username) {
        return em.find(Account.class, username);
    }

    /**
     * Create an account in the database.
     *
     * @param account the account to create in the database for user login
     */
    public void createAccount(Account account) {
        String shaPassword;

        try {
            shaPassword = ShaUtil.hash256(account.getPassword());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        account.setPassword(shaPassword);
        account.setGroupname("Users");
        em.persist(account);
    }

    /**
     * Updates the personal details of a person. This method does not update the
     * list of contacts.
     *
     * @param account an Account object containing the id of the account to
     * update and the account details
     */
    public void updateAccount(Account account) {
        String shaPassword;
        try {
            shaPassword = ShaUtil.hash256(account.getPassword());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        account.setPassword(shaPassword);
        em.merge(account);
    }

    /**
     * Send the verification email using java mail The content contains a link
     * back to the website with username and a hash value as the parameter
     *
     * @param account the account that needs activation
     * @throws NoSuchAlgorithmException
     * @throws MessagingException
     */
    public void sendVerificationMail(Account account) throws NoSuchAlgorithmException, MessagingException {
        String email = account.getEmail();
        String username = account.getUsername();
        String nameHash = ShaUtil.hash256(account.getUsername() + HASH_KEY);

        StringBuffer content = new StringBuffer("Email Verification\n");
        content.append("Please click the following link to activate your account.\n");
        content.append("http://lunar.ml:8080/hungups2-war/faces/activation.xhtml?");
        content.append("username=").append(username).append("&hash=").append(nameHash);

        SendMailUtil.send(email, content.toString());

    }

    /**
     * Update the activation status of an account in the database
     *
     * @param username
     * @param nameHash
     * @return
     */
    public boolean activate(String username, String nameHash) {
        Account account = findAccount(username);
        if (validateHash(username, nameHash)) {
            account.setActive(true);
            em.merge(account);
            return true;
        }
        return false;
    }

    /**
     * Send the reset password email using java mail The content contains a link
     * back to the reset password page of this website with username and a hash
     * value as the parameter
     *
     * @param forgotPasswordAccount
     * @throws NoSuchAlgorithmException
     */
    public void sendResetPasswordEmail(Account forgotPasswordAccount) throws NoSuchAlgorithmException {
        String email = forgotPasswordAccount.getEmail();
        String username = forgotPasswordAccount.getUsername();
        String nameHash = ShaUtil.hash256(forgotPasswordAccount.getUsername() + HASH_KEY);

        StringBuffer content = new StringBuffer("Password Reset Link\n");
        content.append("Please click the following link to reset your password.\n");
        content.append("http://lunar.ml:8080/hungups2-war/faces/reset_password.xhtml?");
        content.append("username=").append(username).append("&hash=").append(nameHash);


        try {
            SendMailUtil.send(email, content.toString());
        } catch (MessagingException ex) {
            Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Validate hash of the username and name hash are matched
     *
     * @param username the username need validation
     * @param nameHash the hash value received
     * @return
     */
    public boolean validateHash(String username, String nameHash) {
        Account account = findAccount(username);
        if (null != account) {
            try {
                String trueHash = ShaUtil.hash256(username + HASH_KEY);
                if (trueHash.equals(nameHash)) {
                    return true;
                }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    /**
     * Update the password of an account in the database
     *
     * @param username
     * @param nameHash
     * @param newPassword
     * @return 
     */
    public boolean resetPassword(String username, String nameHash, String newPassword) {
        Account account = findAccount(username);
        if (null != account) {
            String shaPassword;
            try {
                shaPassword = ShaUtil.hash256(newPassword);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            account.setPassword(shaPassword);
            em.merge(account);
            return true;
        }
        return false;
    }
}
