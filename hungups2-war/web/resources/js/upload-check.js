function checkSubmit(){
    var validateSuccess=true;

    if(document.getElementById("uploadForm:author").value===""){
            document.getElementById("authorErr").innerHTML="Author is required";
            validateSuccess=false;
    }else{
            document.getElementById("authorErr").innerHTML="";
    }
    if(document.getElementById("uploadForm:title").value===""){
            document.getElementById("titleErr").innerHTML="Title is required";
            validateSuccess=false;
    }else{
            document.getElementById("titleErr").innerHTML="";
    }

    if(validateSuccess){
            document.getElementById('imageSubmit').value='Submitting, please wait...';
    }else{
            return false;
    }

}
