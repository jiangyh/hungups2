package au.edu.uts.aip.hungups2.controller;

import au.edu.uts.aip.hungups2.ejb.*;
import au.edu.uts.aip.hungups2.entity.Account;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * This AccountController class is for user login logout, and register
 *
 * @author Yihao Jiang
 *
 */
@Named
@ViewScoped
public class AccountController implements Serializable {

    @EJB
    private AccountBean accountBean;

    private Account account = new Account();
    private String usernameForResetPassword;
    private String nameHashForResetPassword;

    /**
     * Returns the account object that is manipulating by the controller
     *
     * @return
     */
    public Account getAccount() {
        return account;
    }

    /**
     * Load the current logged in account into this controller
     */
    public void loadCurrentAccount() {
        String currentUser = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().toString();
        account = accountBean.findAccount(currentUser);
    }

    /**
     * Calls the EJB accountBean to find the account from the database by its
     * username. Then load the account to this controller
     *
     * @param username the account username looking for
     */
    public void loadAccount(String username) {
        account = accountBean.findAccount(username);
    }

    /**
     * Calls the EJB accountBean to update details of an account in the database
     * the account details are entered by user in the JSF page
     *
     * @return
     */
    public String updateAccount() {
        accountBean.updateAccount(account);
        return "index?faces-redirect=true";
    }

    /**
     * Calls the EJB AccountBean, in order to invoke login method to login
     *
     * @return the redirection to the main page
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.login(account.getUsername(), account.getPassword());
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
        return "index?faces-redirect=true";
    }

    /**
     * Calls the EJB AccountBean, in order to logout from the server
     *
     * @return the redirection to the index page
     */
    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.logout();
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage(e.getMessage()));
        }
        return "index?faces-redirect=true";
    }

    /**
     * Calls the EJB AccountBean to find if the username exists
     *
     * if exists, set the error message the jsf page, then
     *
     * @return the redirection to the register page
     *
     * if the username is available Calls the AccountBean to create an account
     * to save it in the database, then return the redirection to the login page
     */
    public String register() {
        accountBean.createAccount(account);
//        try {
//            accountBean.sendVerificationMail(account);
//        } catch (NoSuchAlgorithmException | MessagingException ex) {
//            Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return "login?faces-redirect=true";
    }

    /**
     * Calls the EJB AccountBean to find if the username exists
     *
     * if exists, set the error message on the jsf page
     *
     * @param f the FacesContext from jsf page
     * @param c the UIComponent from jsf page
     * @param obj the username object use to check exist in database
     */
    public void validateRegisterName(FacesContext f, UIComponent c, Object obj) {
        String name = (String) obj;
        if (name.length() == 0) {
            throw new ValidatorException(new FacesMessage("Please input your username."));
        } else if (!accountBean.findByUsername(name).isEmpty()) {
            throw new ValidatorException(new FacesMessage("Username already exist"));
        }
    }

    /**
     * Validating login names
     * @param f
     * @param c
     * @param obj
     */
    public void validateLoginName(FacesContext f, UIComponent c, Object obj) {
        String name = (String) obj;
        if (name.length() == 0) {
            throw new ValidatorException(new FacesMessage());
        }
    }

    /**
     * Calls the EJB accountBean to activate the account by sending the username
     * and the hash
     *
     * @param username the account username needs to be activated
     * @param nameHash the has value received
     * @return true if the combination is valid and the account is activated
     */
    public boolean activate(String username, String nameHash) {
        return accountBean.activate(username, nameHash);
    }

    /**
     * Calls the EJB accountBean to send an reset password email to the account
     * email
     *
     * @return redirection to the message says email has been sent
     */
    public String sendResetPasswordEmail() {
        // See if the account exists
        Account forgotPasswordAccount = accountBean.findAccount(account.getUsername());
        if (null != forgotPasswordAccount) {
            // See if the emails match
            if (forgotPasswordAccount.getEmail().toLowerCase().equals(account.getEmail().toLowerCase())) {
                try {
                    // Calls EJB to send the email
                    accountBean.sendResetPasswordEmail(forgotPasswordAccount);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return "reset_password_email_sent";
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User and email does not match"));
                return "forgot_password";
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User does not exist"));
            return "forgot_password";
        }
    }

    /**
     * Calls the EJB to validate the hash value and the username
     *
     * @param username the username needs validation
     * @param nameHash the name hash needs validation
     * @return true if the combination is valid
     */
    public boolean validateHash(String username, String nameHash) {
        return accountBean.validateHash(username, nameHash);
    }

    /**
     * Calls the accountBean to reset the account password
     *
     * @param username
     * @param nameHash
     * @return navigation to the success message
     */
    public String resetPassword() {
        if (accountBean.resetPassword(usernameForResetPassword, nameHashForResetPassword, account.getPassword())){
            return "reset_successful?success=1";
        }
        return "reset_successful";
    }
    
    /**
     * Calls on the reset password jsf page to retrieve the parameters from request
     * @param username the account username
     * @param nameHash the received name hash
     */
    public void loadResetPasswordCombination(String username, String nameHash){
        this.usernameForResetPassword = username;
        this.nameHashForResetPassword = nameHash;
    }
}
