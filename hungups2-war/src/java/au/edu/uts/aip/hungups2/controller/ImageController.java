package au.edu.uts.aip.hungups2.controller;

import au.edu.uts.aip.hungups2.ejb.*;
import au.edu.uts.aip.hungups2.entity.Image;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import com.cloudinary.*;
import com.cloudinary.utils.*;
import java.io.*;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.servlet.http.Part;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.apache.commons.io.IOUtils;
import org.cloudinary.json.JSONObject;

/**
 * This ImageController is for CRUD operation of image
 *
 * @author Yihao Jiang
 */
@Named
@ViewScoped
public class ImageController implements Serializable {

    @EJB
    private ImageBean imageBean;

    private Image image = new Image();
    private Part imageFile;
    private static final String API_KEY = "834717547222136";
    private static final String API_SECRET = "FeGAbxLTcfMYJWcmjFBbKS1Qjm4";
    private static final String CLOUD_NAME = "jyh1991";

    public Image getImage() {
        return image;
    }

    public Part getImageFile() {
        return imageFile;
    }

    public void setImageFile(Part imageFile) {
        this.imageFile = imageFile;
    }

    /**
     * Calls the EJB imageBean to find the image by its id from the database
     *
     * @param id the image id looking for
     */
    public void loadImageById(int id) {
        image = imageBean.findImage(id);
    }

    /**
     * Calls the EJB to retrieve all the images from the database
     *
     * @return a collection of all the images in the database
     */
    public List<Image> getAllImages() {
        return imageBean.findAll();
    }

    /**
     * Upload the imageFile that is submitted from the JSF page to Cloudinary
     *
     * @return redirection to the main page
     */
    public String uploadNewImage() {
        // Create cloudinary link
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", CLOUD_NAME,
                "api_key", API_KEY,
                "api_secret", API_SECRET));
        Map uploadResult;
        try {
            // Try to upload and retrieve an result
            uploadResult = cloudinary.uploader().upload(
                    IOUtils.toByteArray(imageFile.getInputStream()),
                    ObjectUtils.emptyMap());
        } catch (IOException ex) {
            Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        // set the image url to the image object
        image.setUrl((String) uploadResult.get("url"));
        String currentUser = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().toString();
        // calls the EJB to update the image url and the upload user in the database
        imageBean.uploadImage(image, currentUser);
        return "index?faces-redirect=true";
    }

    /**
     * Calls the EJB to Update the details of an image in the database
     *
     * @return redirection to the manage image page
     */
    public String updateImage() {

        // For normal users, check image owner
        if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("Users")) {
            String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().toString();
            if (image.getAccount().getUsername().equals(username)) {
                imageBean.updateImage(image);
                return "manage_image?faces-redirect=true";
            }
            // For administrator
        } else if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("Admins")) {
            imageBean.updateImage(image);
            return "admin_manage?faces-redirect=true";
        }
        return "index?faces-redirect=true";
    }

    /**
     * Calls the EJB to delete the image from the database
     *
     * @return redirection to the manage image page
     */
    public String deleteImage() {

        // For normal users, check image owner
        if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("Users")) {
            String username = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().toString();
            if (image.getAccount().getUsername().equals(username)) {
                imageBean.deleteImage(image);
                return "manage_image?faces-redirect=true";
            }
            // For administrator
        } else if (FacesContext.getCurrentInstance().getExternalContext().isUserInRole("Admins")) {
            imageBean.deleteImage(image);
            return "admin_manage?faces-redirect=true";
        }
        return "index?faces-redirect=true";
    }

    /**
     * Check if the image is the correct size Check if the uploaded file is an
     * image
     *
     * @param context
     * @param component
     * @param obj
     */
    public void imageValidate(FacesContext context, UIComponent component, Object obj) {
        Part file = (Part) obj;
        // Check if there is something uploaded
        if (file == null) {
            throw new ValidatorException(new FacesMessage("Please select image"));
        }
        // Check if the file size is correct
        if (file.getSize() > 4000000) {
            throw new ValidatorException(new FacesMessage("Uploaded image is too large"));
        }
        // Check if it is an image
        if ((!"image/jpeg".equals(file.getContentType()))
                && (!"image/bmp".equals(file.getContentType()))
                && (!"image/png".equals(file.getContentType()))) {
            throw new ValidatorException(new FacesMessage("Uploaded file is not image"));
        }
    }

    /**
     * Retrieve Cloudinary information for uploading images
     * @return 
     */
    public String getCloudinaryInformation() {

        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("834717547222136", "FeGAbxLTcfMYJWcmjFBbKS1Qjm4".toCharArray());
            }
        });
        Client client = ClientBuilder.newClient();
        WebTarget adminResource = client.target("https://api.cloudinary.com/v1_1/jyh1991/usage");
        Invocation.Builder builder = adminResource.request(MediaType.TEXT_PLAIN);
        String result = builder.get(String.class);
        JSONObject resultJson = new JSONObject(result);

        JSONObject transformations = (JSONObject) resultJson.get("transformations");
        JSONObject images = (JSONObject) resultJson.get("objects");
        JSONObject bandwidth = (JSONObject) resultJson.get("bandwidth");
        JSONObject storage = (JSONObject) resultJson.get("storage");

        StringBuffer info = new StringBuffer();
        info.append("Transformations: ").append(transformations.get("used_percent")).append("%\n  ")
                .append("Images: ").append(images.get("used_percent")).append("%\n  ")
                .append("Bandwidth: ").append(bandwidth.get("used_percent")).append("%\n  ")
                .append("Storage: ").append(storage.get("used_percent")).append("%\n  ");

        return info.toString();
    }
}
